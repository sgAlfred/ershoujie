# 二手街项目

前端：handlebars / bootstrap /jQuery

后端：express / mysql5.7

## 必要环境：

1）安装 nodejs，下载地址：http://nodejs.cn/download/

2）确保 mysql 环境已经可以使用。创建数据库 `test_ershoujie` 或什么名字，同时导入 /database/test_ershoujie.sql 到该数据库中。
修改 /dbHandle/index.js ，使得数据库密码等信息正确。。


## 关于项目

采用的是 轻量级Express 框架(文档地址：http://www.expressjs.com.cn/)，前端使用 Bootstrap3 布局(文档地址：ttps://v3.bootcss.com/)。

二者的特性及具体使用可见文档说明。


## 如何搭建？

1)、安装 express，执行 `npm install -g generator-express`

2)、运行 `express -h`，如果出现以下则说明安装成功了。

```
➜  ～ express -h

  Usage: express [options] [dir]

  Options:

    -h, --help           output usage information
        --version        output the version number
    -e, --ejs            add ejs engine support
        --pug            add pug engine support
        --hbs            add handlebars engine support
    -H, --hogan          add hogan.js engine support
    -v, --view <engine>  add view <engine> support (dust|ejs|hbs|hjs|jade|pug|twig|vash) (defaults to jade)
    -c, --css <engine>   add stylesheet <engine> support (less|stylus|compass|sass) (defaults to plain css)
        --git            add .gitignore
    -f, --force          force on non-empty directory
```

3)、切换到项目目录下，执行 `express --hbs -v hbs ershoujie`

该命令行的意思是：在 文件夹ershoujie 下创建一个 express 工程，使用 handlebars 作为模版引擎。

4)、切换到 ershoujie 目录，执行 `npm install`，安装项目依赖。

5)、由于需要用到 mysql 和 session，因此需要执行 `npm install mysql express-session`

6)、一切准备完毕，只要开始写代码就好了～～


## 如何运行？

1)、切换到该目录下，直接运行 `npm start`，浏览器打开 `127.0.0.1:3000`，就可以看到界面；

2)、如果出错，则先运行 `npm install`，安装依赖成功后，再执行上一步。

