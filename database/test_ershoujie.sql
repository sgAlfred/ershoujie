/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : localhost
 Source Database       : test_ershoujie

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : utf-8

 Date: 05/10/2018 00:11:42 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `wx_cart`
-- ----------------------------
DROP TABLE IF EXISTS `wx_cart`;
CREATE TABLE `wx_cart` (
  `goods_id` bigint(10) NOT NULL COMMENT '商品id',
  `user_id` bigint(10) NOT NULL COMMENT '用户id',
  `amount` int(2) DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  `createtime` datetime DEFAULT '2018-05-04 00:00:00' COMMENT '创建时间',
  `updatetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`goods_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车表';

-- ----------------------------
--  Table structure for `wx_goods`
-- ----------------------------
DROP TABLE IF EXISTS `wx_goods`;
CREATE TABLE `wx_goods` (
  `goods_id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `goods_classify_id` bigint(12) NOT NULL COMMENT '商品分类',
  `goods_name` varchar(30) NOT NULL COMMENT '商品名称',
  `goods_detail` text NOT NULL COMMENT '商品描述',
  `goods_image` varchar(300) NOT NULL DEFAULT '/iamges/goods/default.jpg' COMMENT '商品预览照片',
  `goods_price` float NOT NULL COMMENT '商品价格',
  `goods_number` bigint(4) DEFAULT '1' COMMENT '商品数量',
  `goods_school_id` bigint(20) DEFAULT NULL COMMENT '学校id',
  `goods_trade_place` varchar(100) DEFAULT NULL COMMENT '交易地点',
  `user_id` bigint(11) NOT NULL COMMENT '用户ID',
  `goods_view_number` bigint(10) DEFAULT '0' COMMENT '物品浏览量',
  `goods_integral_number` int(11) DEFAULT '0' COMMENT '物品积分',
  `goods_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '物品状态--0/在售/1已售/2下架',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
--  Table structure for `wx_goods_classify`
-- ----------------------------
DROP TABLE IF EXISTS `wx_goods_classify`;
CREATE TABLE `wx_goods_classify` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '商品分类id',
  `parentid` bigint(10) NOT NULL DEFAULT '-1' COMMENT '父分类',
  `name` varchar(20) NOT NULL COMMENT '分类名称',
  `createtime` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- ----------------------------
--  Table structure for `wx_goods_comment`
-- ----------------------------
DROP TABLE IF EXISTS `wx_goods_comment`;
CREATE TABLE `wx_goods_comment` (
  `comment_id` int(12) NOT NULL AUTO_INCREMENT,
  `comment_goods_id` varchar(32) NOT NULL COMMENT '评论商品id',
  `comment_user_id` varchar(32) NOT NULL COMMENT '评论用户id',
  `comment_username` varchar(20) NOT NULL COMMENT '评论用户名',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '评论内容',
  `pid` int(12) DEFAULT '-1' COMMENT '回复评论id',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '评论时间',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='商品评论表';

-- ----------------------------
--  Table structure for `wx_province`
-- ----------------------------
DROP TABLE IF EXISTS `wx_province`;
CREATE TABLE `wx_province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '省份id',
  `province_name` varchar(255) NOT NULL COMMENT '省份名称',
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='省份表';

-- ----------------------------
--  Table structure for `wx_school`
-- ----------------------------
DROP TABLE IF EXISTS `wx_school`;
CREATE TABLE `wx_school` (
  `school_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学校id',
  `school_name` varchar(255) NOT NULL COMMENT '学校名称',
  `school_pro_id` int(11) NOT NULL COMMENT '省份id',
  `school_schooltype_id` int(11) NOT NULL COMMENT '学校类型(1: 本科高校; 2: 高职院校; 3: 独立学院; 4: 其他)',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`school_id`),
  KEY `PK_school_province` (`school_pro_id`),
  KEY `PK_school_schooltype` (`school_schooltype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2735 DEFAULT CHARSET=utf8 COMMENT='学校表';

-- ----------------------------
--  Table structure for `wx_user`
-- ----------------------------
DROP TABLE IF EXISTS `wx_user`;
CREATE TABLE `wx_user` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(20) NOT NULL COMMENT '登录用户名，全局唯一',
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `salt` char(4) NOT NULL DEFAULT '' COMMENT '掩码',
  `password` char(32) NOT NULL COMMENT 'md5密码',
  `gender` varchar(8) DEFAULT NULL COMMENT '性别',
  `face` varchar(500) DEFAULT NULL COMMENT '头像',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '用户状态/1允许/0禁止',
  `email` varchar(55) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(22) DEFAULT 'null' COMMENT '手机号',
  `openid` varchar(64) DEFAULT NULL COMMENT '微信openid',
  `qq` varchar(12) DEFAULT 'null' COMMENT 'qq号',
  `address` varchar(300) DEFAULT 'null' COMMENT '地址',
  `logindate` datetime DEFAULT '2015-02-18 00:00:00' COMMENT '用户最后登陆时间',
  `createtime` datetime DEFAULT '2018-05-04 00:00:00' COMMENT '创建时间',
  `updatetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  KEY `INDEX_USERNAME` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COMMENT='用户表';

SET FOREIGN_KEY_CHECKS = 1;
