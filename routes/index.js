var express = require('express');
var router = express.Router();

var utils = require('./utils')

var pageSize = 12   // 每页的条数

/* GET home page. */
router.get('/', function(req, res, next) {

  // 参数
  var pageNo = req.query.pageNo || 1    // get请求
  var offset = (pageNo-1) * pageSize;   // 数据偏移量

  var classifyid = req.query.classifyid      // 商品类别
  var goodsnameLike = req.query.keyword    // 商品名称模糊搜索

  // 动态的 sql 条件！
  var conditionArr = ['WHERE `goods_status` != "2"']
  if (classifyid) {
    conditionArr.push('`goods_classify_id` = '+classifyid)
  }
  if (goodsnameLike) {
    conditionArr.push('`goods_name` LIKE "%'+goodsnameLike+'%"')
  }
  var condition = conditionArr.join(' AND ')

  var result

  // 还要统计总数！
  global.sqlConnection.query({
    sql: 'SELECT count(*) AS totalcount FROM `wx_goods`' + condition,
    values: []
  }, function(error, response) {
    console.log('查询货品总数--', error, response);
    if (error) {
      result = {
        code: 2,
        message: '获取商品总数失败--' + error.message
      }
      utils.appResponse(res, result)
    }

    var totalcount = response[0].totalcount

    global.sqlConnection.query({
      sql: 'SELECT * FROM `wx_goods`' + condition +' ORDER BY createtime DESC LIMIT ?, ?',
      values: [offset, pageSize]
    }, function(error, response) {
      console.log('查询货品信息--', error, response);
      if (error) {
        result = {
          code: 2,
          message: 'index获取货品列表失败失--' + error.message
        }
        utils.appResponse(res, result)
      }
      var goods = response

      global.sqlConnection.query({
        sql: 'SELECT * FROM `wx_goods_classify`',
        value: []
      }, function(error, response) {
        if (error) {
          result = {
            code: 2,
            message: 'index获取货品分类信息失败--' + error.message
          }
          utils.appResponse(res, result)
        }
        var classify = response

        var page = pageFormat(totalcount, pageNo, pageSize)

        // console.log('分页信息--', totalcount, pageNo, page)

        res.render('index', { title: '校园二手街', page: page, goods: goods, classify: classify });

      })
    })
  })
});


// 消息界面
router.get('/message', function(req, res, next) {
  res.render('message', { title: '消息' });
})

// 个人主页，如果未登录，则显示登录按钮
// 登录后自动跳回原先的界面
router.get('/home', function(req, res, next) {
  var username = req.session.username

  var result

  if (username) {

    var userid = req.session.userinfo.id

    var statusMap = ['在售', '已售', '下架']

    global.sqlConnection.query({
      sql: 'SELECT * FROM `wx_goods_classify`',
      value: []
    }, function(error, response) {
      if (error) {
        result = {
          code: 2,
          message: 'home获取货品分类信息失败--' + error.message
        }
        utils.appResponse(res, result)
      }
      var classify = response

      // 已经发布的商品
      global.sqlConnection.query({
        sql: 'SELECT * FROM `wx_goods` WHERE `user_id` = ? AND `goods_status` != "2" ORDER BY `createtime` DESC',
        values: [ userid ]
      }, function(error, response) {
        if (error) {
          result = {
            code: 2,
            message: 'home获取已发布的货品信息失败--' + error.message
          }
          utils.appResponse(res, result)
        }
        var mygoods = []
        response.forEach(function(value) {
          value.status = statusMap[value.goods_status]
          mygoods.push(value)
        })

        // 购物车商品
        global.sqlConnection.query({
          sql: 'SELECT * FROM `wx_cart` AS cart LEFT JOIN wx_goods AS goods ' +
          'ON cart.goods_id = goods.goods_id WHERE cart.`user_id` = ? AND cart.`deleted` = 0',
          values: [ userid ]
        }, function(error, response) {
          if (error) {
            result = {
              code: 2,
              message: 'home获取购物车信息失败--' + error.message
            }
            utils.appResponse(res, result)
          }
          var mycarts = []
          response.forEach(function(value) {
            value.status = statusMap[value.goods_status]
            mycarts.push(value)
          })

          res.render('home', { title: '我的主页', userinfo: req.session.userinfo, classify: classify,
            mygoods: mygoods, mycarts: mycarts });
        })
      })

    })

  } else {
    res.redirect("/users/signin");
  }
})

module.exports = router;

/**
 * 获取分页信息
 *
 * @param totalcount  总条数
 * @param pageNo      当前页面
 * @param pageSize    每页的数量
 */
function pageFormat(totalcount, pageNo, pageSize) {
  pageNo = parseInt(pageNo) || 1
  pageSize = parseInt(pageSize) || pageSize

  // 总页数
  var totalPage = Math.ceil(totalcount/pageSize)

  var arr = []  // 返回前端渲染的数据

  // 跳到首页
  var toFirst = {
    index: 1,
    text: '&laquo;',
    clickable: 1 !== pageNo     // 是否可以点击
  }
  arr.push(toFirst)

  // 跳到前页
  var toPrev = {
    index: pageNo-1,
    text: '&lsaquo;',
    clickable: 1 !== pageNo
  }
  arr.push(toPrev)

  // 跳到特定页面
  for (var i = 1; i <= totalPage; i++) {
    var toPage = {
      index: i,
      text: i,
      clickable: i !== pageNo
    }
    arr.push(toPage)
  }

  var toNext = {
    index: pageNo+1,
    text: '&rsaquo;',
    clickable: totalPage !== pageNo
  }
  arr.push(toNext)

  var toLast = {
    index: totalPage,
    text: '&raquo;',
    clickable:  totalPage !== pageNo
  }
  arr.push(toLast)

  return arr
}
