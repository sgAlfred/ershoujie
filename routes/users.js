var express = require('express');
var router = express.Router();

var utils = require('./utils')

/**
 * 渲染用户注册界面
 */
router.get('/signup', function(req, res, next) {
  res.render('signup', { title: '用户注册' });
})

/**
 * 渲染用户登录界面
 */
router.get('/signin', function(req, res, next) {
  res.render('signin', { title: '用户登录' });
})

// 注册新用户
router.post('/signup', function (req, res, next) {
  var username = req.body.username;
  var rawPassword = req.body.password;

  var result = {}

  global.sqlConnection.query({
    sql: 'SELECT * FROM `wx_user` WHERE `username` = ? LIMIT 1',
    values: [username]
  }, function(error, response) {

    console.log('注册查询用户是否存在--', error, response)

    if (error) {
      result = {
        code: 2,
        message: '查询数据库出错'
      }
      utils.appResponse(res, result)
    } else if (response.length > 0) {
      result = {
        code: 2,
        message: '用户名已存在！'
      }
      utils.appResponse(res, result)
    } else {

      // 掩码
      var salt = utils.getRandomStr(4);
      var password = utils.createMd5(salt+rawPassword)

      var insert = {
        username: username,
        password: password,
        salt: salt,
        gender: req.body.gender,
        email: req.body.email,
        mobile: req.body.mobile,
        qq: req.body.qq
      }

      global.sqlConnection.query("INSERT INTO `wx_user` SET ? ", insert, function(error, response, fields) {
        console.log('注册新建用户--', error, response)
        if (error) {
          result = {
            code: 2,
            message: '数据库创建新用户失败--' + error.message
          }
          utils.appResponse(res, result)
        }
        if (response.insertId) {
          result = {
            code: 0,
            message: '创建用户成功',
            data: response
          }
          utils.appResponse(res, result)
        }
      })
    }
  })
})

// 登录用户
router.post('/signin', function (req, res, next) {
  var username = req.body.username;
  var rawPassword = req.body.password;

  var result = {}

  global.sqlConnection.query({
    sql: 'SELECT * FROM `wx_user` WHERE `username` = ? LIMIT 1',
    values: [username]
  }, function(error, response) {

    // console.log('登录查询用户--', error, response)

    if (error) {
      result = {
        code: 2,
        message: '查询数据库出错'
      }
      utils.appResponse(res, result)
    } else if (0 === response.length) {
      result = {
        code: 2,
        message: '用户名不存在！'
      }
      utils.appResponse(res, result)
    } else {

      var userinfo = response[0];

      // md5密码
      var password = utils.createMd5(userinfo.salt+rawPassword)

      if (password !== userinfo.password) {
        result = {
          code: 2,
          message: '用户密码错误！'
        }
        utils.appResponse(res, result)
      } else {
        // 设置 session
        req.session.userinfo = userinfo
        req.session.username = username;
        // 返回用户信息
        result = {
          code: 0,
          messge: '登录成功',
          data: userinfo
        }
        utils.appResponse(res, result)
      }
    }
  })
})

// 退出登录
router.post('/signout', function(req, res, next) {
  req.session.username = null
  req.session.userinfo = null
  var result = {
    code: 0,
    messge: '注销成功',
    data: 'success'
  }
  utils.appResponse(res, result)
})

/**
 * 更新用户信息
 */
router.post('/update', function(req, res, next) {
  var userid = req.session.userinfo.id

  // 需要更新的字段，因为是界面单个更新的，因此，直接这样
  var upadteinfo = req.body.name + ' = "' + req.body.value + '"'

  // 更新～
  global.sqlConnection.query({
    sql: 'UPDATE `wx_user` SET ' + upadteinfo + ' WHERE id = ? ',
    values: [userid]
  }, function(error, response) {

    console.log('更新用户信息--', error, response)

    var result
    if (error) {
      result = {
        code: 2,
        message: '更新用户信息失败！'
      }
      utils.appResponse(res, result)
    }
    result = {
      code: 0,
      data: response.affectedRows
    }
    req.session.userinfo[req.body.name] = req.body.value
    utils.appResponse(res, result)
  })

})

module.exports = router;
